name := "project"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "org.apache.kafka" %% "kafka-streams-scala" % "2.0.1" exclude("javax.ws.rs", "javax.ws.rs-api"),
  "javax.ws.rs" % "javax.ws.rs-api" % "2.1.1" artifacts(Artifact("javax.ws.rs-api", "jar", "jar")),

  "io.github.embeddedkafka" %% "embedded-kafka" % "2.1.0" % "test",
  "io.github.embeddedkafka" %% "embedded-kafka-streams" % "2.1.0" % "test",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)

