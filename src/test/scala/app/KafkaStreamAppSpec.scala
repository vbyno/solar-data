package app

import org.scalatest.FlatSpec
import net.manub.embeddedkafka.EmbeddedKafkaConfig
import net.manub.embeddedkafka.streams.EmbeddedKafkaStreamsAllInOne
import net.manub.embeddedkafka.Codecs._
import net.manub.embeddedkafka.ConsumerExtensions._
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.streams.scala.Serdes
import org.apache.kafka.common.serialization.Deserializer

class KafkaStreamAppSpec extends FlatSpec with EmbeddedKafkaStreamsAllInOne {
  val inTopic = "text"
  val outTopic = "line-length"

  implicit val config = EmbeddedKafkaConfig(kafkaPort = 7000, zooKeeperPort = 7001)

  it should "A Kafka streams test" in {
    runStreams(Seq(inTopic, outTopic), (new KafkaStreamApp).lineLengths()) {
      publishToKafka(inTopic, "world")
      publishToKafka(inTopic, "bar")
      publishToKafka(inTopic, "yaz")

      implicit val intDeserializer: Deserializer[Int] = Serdes.Integer.deserializer
      implicit val intValueCrDecoder: ConsumerRecord[String, Int] => Int = _.value()

      withConsumer[String, Int, Unit] { consumer =>
        val consumedMessages = consumer.consumeLazily[Int](outTopic)

        assert(consumedMessages.take(2) == Seq(5, 3))
      }
    }
  }
}