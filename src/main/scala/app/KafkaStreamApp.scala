package app

import org.apache.kafka.streams.scala.{StreamsBuilder, Serdes}
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.{KafkaStreams, Topology, StreamsConfig}
import org.apache.kafka.clients.consumer.ConsumerConfig
import java.util.Properties

class KafkaStreamApp {
  import Serdes._

  def lineLengths(): Topology = {
    val builder = new StreamsBuilder

    builder
      .stream[String, String]("text")
      .mapValues(_.size)
      .to("line-length")

    builder.build()
  }
}

object KafkaStreamApp extends App {
  val props = {
    val p = new Properties()

    p.put(StreamsConfig.APPLICATION_ID_CONFIG, "solar-data-1")
    p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "kafka:9092")
    p.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    p
  }

  val topology = (new KafkaStreamApp).lineLengths()
  val streams = new KafkaStreams(topology, props)

  streams.cleanUp()
  streams.start()

  sys.ShutdownHookThread {
    streams.close()
  }
}
