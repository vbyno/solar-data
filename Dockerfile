FROM hseeberger/scala-sbt:8u181_2.12.8_1.2.8

ENV APP_HOME /solar-data
RUN mkdir -p $APP_HOME && mkdir -p $APP_HOME/out
WORKDIR $APP_HOME

COPY project/ project/
COPY build.sbt build.sbt
RUN sbt update

COPY . .
