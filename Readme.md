## Solar Data

Look at [assignment](https://drive.google.com/file/d/1RTeV0p3nfXF0MJIlwc35ov1xAny_ddo3/view) for details

### Running application
1. Stop kafka and zookeeper on local machine: `confluent stop`
2. Start docker containers: `docker-compose up`
3. (in a new terminal window): `docker-compose exec ksql-cli ksql http://ksql-server:8088`
4. Recheck if ksql console works: `show tables;`

### Running tests

##### If containers are running: 
    docker-compose exec app sbt test
##### If containers are not running: 
    docker-compose run app sbt test
    
### Stop application
    docker-compose down
